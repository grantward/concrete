#!/usr/bin/env python3
# Concrete
# Copyright (C) 2022  Grant Ward
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.0 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""CLI program providing calculator."""

import argparse

from data import bundled
from src import cli


def main():
    """Entry point for calculator."""
    parser = argparse.ArgumentParser(
        description='Calulcate necessary concrete / mix components')
    parser.add_argument(
        '-R', '--no-round',
        dest='round',
        action='store_false',
        help='Do not round numbers in output',
    )
    subparsers = parser.add_subparsers(
        title='Program modes',
        dest='mode',
        required=True,
    )

    mix_parser = subparsers.add_parser(
        'mix',
        help='Compute mixture ratios of concrete to '
             'water for given coverage volume')
    mix_parser.add_argument(
        '-c', '--concrete-type',
        dest='concrete',
        choices=bundled.data.coverage.keys(),
        default='quikrete-high-strength',
        help='Type of concrete to use')
    mix_parser.add_argument(
        '-v', '--volume-needed',
        dest='volume',
        type=float,
        required=True,
        help='Volume of concrete needed (in cubic feet)')

    project_parser = subparsers.add_parser(
        'volume',
        help='Get the volume of concrete needed for various common projects.',
    )
    project_parser.add_argument(
        '-q', '--quantity',
        default=1,
        type=float,
        help='Quantity of projects to complete, (ie. 10 fence posts)',
    )
    projects = project_parser.add_subparsers(
        title='Projects available',
        dest='project',
        required=True,
    )
    round_post = projects.add_parser(
        'post', help='Set a round post in concrete')
    round_post.add_argument(
        '--post-diameter',
        type=float,
        required=True,
        help='Diamater of post to set',
    )
    round_post.add_argument(
        '--hole-diameter',
        type=float,
        required=True,
        help='Diamater of hole to set post in',
    )
    round_post.add_argument(
        '-d', '--depth',
        type=float,
        required=True,
        help='Depth to set post',
    )

    args = parser.parse_args()

    funcs = {
        'mix': cli.mix_portions,
        'volume': cli.project_volume,
    }
    output = funcs[args.mode](args)
    print(output)


if __name__ == '__main__':
    main()
