# Concrete
# Copyright (C) 2022  Grant Ward
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.0 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Provides all the packaged JSON data."""

import json
import os


BASE_DIR = 'data'


def read_data(name):
    """Load the JSON data associated with name."""
    with open(
            os.path.join(BASE_DIR, f'{name}.json'),
            'r',
            encoding='utf8') as data_f:
        return json.load(data_f)


class Data:
    """Interface into bundled JSON data."""
    files = [
        'coverage',
        'density',
        'water_ratio',
    ]

    def __getattr__(self, attr):
        """Return the processed data for a data file if it exists,
        otherwise read it in and process.
        """
        if attr in self.files:
            if attr in self.__dict__:
                return self.__dict__[attr]
            self.__setattr__(attr, read_data(attr))
        return getattr(self, attr)


data = Data()


if __name__ == '__main__':
    pass
