# Concrete Data

## Units
### Coverage
Cubic feet per Lb when mixed and cured properly. This is the end result
of pouring concrete.
### Density
Lb per cubic foot
### Water Ratio
Lb water / Lb concrete
