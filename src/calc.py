# Concrete
# Copyright (C) 2022  Grant Ward
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.0 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Provides calclation functions."""

import math

from data import bundled


GALLONS_PER_CUBIC_FOOT = 7.480543


def weight_concrete_required(
        concrete_type: str, coverage_volume: float) -> float:
    """Return the weight of concrete mix required for specified
    coverage volume.
    """
    coverage_ratio = bundled.data.coverage[concrete_type]
    return coverage_volume / coverage_ratio


def weight_water_required(
        concrete_type: str, concrete_weight: float) -> float:
    """Return the weight of water needed for given weight of concrete
    mix.
    """
    water_ratio = bundled.data.water_ratio[concrete_type]
    return concrete_weight * water_ratio


def gallons_from_weight(name: str, weight: float) -> float:
    """Return the volume (gallons) of substance from weight given."""
    density = bundled.data.density[name]
    return GALLONS_PER_CUBIC_FOOT * (weight / density)


def post_in_hole(post_diameter: float,
                 hole_diameter: float,
                 depth: float) -> float:
    """Return the volume of concrete needed to set a post in a hole
    with specified characteristics.

    All units are inches, cubic feet is returned.
    """
    hole_volume = math.pi * math.pow((hole_diameter/2), 2) * depth
    post_volume = math.pi * math.pow((post_diameter/2), 2) * depth
    return (hole_volume - post_volume) / 12 / 12 / 12


def round_number(number: float) -> str:
    """Round a number for output to user."""
    if number > 10:
        return f'{math.ceil(number)}'
    num_str = str(number)
    if len(num_str) > 3:
        third = int(num_str[2]) + 1
        num_str = f'{num_str[0:2]}{third}'
    return num_str[0:3]


if __name__ == '__main__':
    pass
