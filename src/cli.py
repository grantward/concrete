# Concrete
# Copyright (C) 2022  Grant Ward
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.0 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Converts user inputs into concrete data."""

import string

from src import calc


class ConcreteFormatter(string.Formatter):
    """Provides smoother display of floating point numbers."""
    def format_field(self, value, format_spec):
        if isinstance(value, float):
            return calc.round_number(value)
        return super().format_field(value, format_spec)


def mix_portions(args) -> str:
    """Calculates mix percnetages and returns a string to
    print for the user.
    """
    formatter = ConcreteFormatter() if args.round else string.Formatter()
    concrete_pretty = args.concrete.replace('-', ' ')
    concrete_weight = calc.weight_concrete_required(
        args.concrete, args.volume)
    water_weight = calc.weight_water_required(
        args.concrete, concrete_weight)
    concrete_gallons = calc.gallons_from_weight(
        args.concrete, concrete_weight)
    water_gallons = calc.gallons_from_weight('water', water_weight)
    lines = [
        'For {args.volume} cubic feet of {concrete_pretty} concrete, mix:',
        '{concrete_weight} Lb. concrete ({concrete_gallons} gallons)',
        '{water_weight} Lb. water ({water_gallons} gallons)',
    ]
    return formatter.format('\n'.join(lines), **locals())


def project_volume(args) -> str:
    """Calculates needed volume of coverage for given project
    parameters.
    """
    formatter = ConcreteFormatter() if args.round else string.Formatter()
    funcs = {
        'post': calc.post_in_hole,
    }
    func_args = {
        'post': {
            'post_diameter': 'post_diameter',
            'hole_diameter': 'hole_diameter',
            'depth': 'depth',
        }
    }
    pretty = {
        'post': 'round post(s) set in concrete',
    }
    built_args = {
        key: args.__dict__[value]
        for key, value in func_args[args.project].items()
    }
    volume = funcs[args.project](**built_args)
    volume *= args.quantity
    lines = [
        f'For {args.quantity} {pretty[args.project]}, you need:',
        '{volume} cubic feet of concrete.',
    ]
    return formatter.format('\n'.join(lines), **locals())


if __name__ == '__main__':
    pass
